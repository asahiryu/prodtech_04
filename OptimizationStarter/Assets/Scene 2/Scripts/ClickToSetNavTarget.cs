﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    Camera cameraObj;
    NavMeshAgent navAgent;

    private void Start()
    {
        //find our Main Camera object in the hierarchy 
        cameraObj = GameObject.Find( "Main Camera" ).GetComponent<Camera>();
        navAgent = GetComponent<NavMeshAgent>();
    }


    // Update is called once per frame
    void Update ()
    {

        if(Input.GetButtonDown("Fire1"))
        {
            //create raycast
            RaycastHit hit = new RaycastHit();
            //if our raycast aimed towards our mouse coordinates hits a "Ground" object, set our NavMeshAgent's destination to our mouse coordinates
            if (Physics.Raycast(cameraObj.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
            {
                navAgent.destination = hit.point;
            }
        }


	}
}
