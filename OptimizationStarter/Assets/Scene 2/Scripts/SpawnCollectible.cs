﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    //Collectible game object variable
    public GameObject Collectible;
    //current spanwed Collectible game object variable
    private GameObject m_currentCollectible;

    // Update is called once per frame
    void Update()
    {   
        //if there is no collectable stored
        if ( m_currentCollectible == null )
        {
            //instantiate our collectable object at a randomly chosen child object's position/rotation
            m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.GetChildCount() ) ).position, Collectible.transform.rotation );
        }
    }
}
