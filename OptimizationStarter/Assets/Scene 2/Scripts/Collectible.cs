﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    //collectable radius variable
    public float Radius;

    public void Update()
    {
        /*
        //create an array of colliders for all colliders inside our sphere of x radius at our collectable object's position
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
        //for every object in the array starting at the first element
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            //if the object's tag is "Player", destroy this collectable object
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                Destroy( this.gameObject );
            }
        } 
        */
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }

}
