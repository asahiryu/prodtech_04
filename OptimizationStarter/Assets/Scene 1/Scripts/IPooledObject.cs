﻿using UnityEngine;

//interface allows us to specify types and functions for objects
public interface IPooledObject 
{

    void OnObjSpawn();

}
