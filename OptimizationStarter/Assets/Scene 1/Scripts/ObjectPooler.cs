﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        //store tag, prefab of object in pool, and size of pool
        public string tag;
        public GameObject prefab;
        public int size;
    }

    public static ObjectPooler Instance;

    private void Awake()
    {
        Instance = this;
    }

    public GameObject cameraObj;
    Vector3 FireDirection;

    //list of pools
    public List<Pool> pools;

    //create a dictionary to store our spheres
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    // Start is called before the first frame update
    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        //loop through pools
        foreach(Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            //instantiate pool of objects until pool is full
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            //add pool to dictionary
            poolDictionary.Add(pool.tag, objectPool);
        }
    }

    //take unactive objects and spawn them
    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        //if invalid pool tag, error
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("'" + tag + "'" + " pool does not exist.");
            return null;
        }



        //get prefab/queue we want to spawn(tag) and get first element of that queue and set it to our object to spawn
        GameObject objToSpawn = poolDictionary[tag].Dequeue();


        objToSpawn.SetActive(true);
        objToSpawn.transform.position = position;
        objToSpawn.transform.rotation = rotation;

        FireDirection = cameraObj.GetComponent<FirePrefab>().FireDirection;

        objToSpawn.GetComponent<Rigidbody>().velocity = FireDirection * 15;


        //add back into our queue
        poolDictionary[tag].Enqueue(objToSpawn);

        return objToSpawn;
    }


}
