﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public Vector3 FireDirection;

    ObjectPooler objPooler;

    Camera mainCamera;
    Vector3 thisPosition;

    bool fired = false;
    public float fireRate = 1f; 


    private void Start()
    {
        objPooler = ObjectPooler.Instance;
        mainCamera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        //if the fire button is pressed
        if ( Input.GetButtonDown( "Fire1" ))
        {
            InvokeRepeating("SpawnObject", 0f, fireRate);
        }
        if ( Input.GetButtonUp("Fire1"))
        {
            CancelInvoke();
        }

    }

    void SpawnObject()
    {
        //store mouse coordinates to Vector3 "clickPoint" 
        Vector3 clickPoint = mainCamera.ScreenToWorldPoint(Input.mousePosition + Vector3.forward);
        //subtract current FirePrefab position from our mouse coordinate position to get direction & normalize
        FireDirection = clickPoint - this.transform.position;

        //spawn the spheres from a pool
        objPooler.SpawnFromPool("RedSphere", this.transform.position, Quaternion.identity);

        //GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
        ////move the instatiated prefab towards the mouse coordinates at FireSpeed 
        //prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
    }


}
